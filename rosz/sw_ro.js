var CACHE_NAME = "${HASH}";
var URLS_CACHE = ${PAGE_LIST};  // AEM will place array of paths into this binding

//----------------------------------------------------------------
//roche version
var urlsToCache = URLS_CACHE.slice(0);
urlsToCache.push("/");

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log("addAll");
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('activate', function(e) {
    console.log('[ServiceWorker] Activated');

    e.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(cacheNames.map(function(thisCacheName) {
                if (thisCacheName !== CACHE_NAME) {
                    console.log('[ServiceWorker] Removing Cached Files from Cache - ', thisCacheName);
                    return caches.delete(thisCacheName);
                }
            }));
        })
    );

});

self.addEventListener('fetch', function (evt) {
    console.log("/service worker:/ responding to fetch ");
    evt.respondWith(
        caches.match(evt.request)
            .then(function (response) {
                var clonedRq = evt.request.clone();
                if (response) {
                    console.log("response found in cache");
                    return response;
                }
                return fetch(clonedRq).then(function (res) {
                    if (!res) {
                        console.log("no response from fetch from online after checking cache");
                        return res;
                    }
                    var clonedRes = res.clone();
                    caches.open(CACHE_NAME).then(function (cache) {
                        cache.put(evt.request, clonedRes);
                        console.log("service worker has just cached new data with cache.put");
                        return res;
                    });
                })
            })
            .catch(function (error) {
                console.log("error while fetching new data for cache (or storing it)", error);
            })

    );
});