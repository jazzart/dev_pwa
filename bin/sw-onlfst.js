let urlsToCache = ["/",
    "/index.html",
    "/contact.html",
    "/portfolio.html",
    "/pricing.html",
    "/tour.html"
];
const CACHE_NAME = "cache-vr1";
const CACHE_NAME_DYNAMIC = "cachedynamic-vr1";

function inUrlArray(str, arr) {
    let cachePath;
    if (str.indexOf(self.origin) === 0) { // request targets domain where we serve the page from (i.e. NOT a CDN)
        // console.log('matched ', str);
        cachePath = str.substring(self.origin.length); // take the part of the URL AFTER the domain (e.g. after localhost:8080)
    } else {
        cachePath = str; // store the full request (for CDNs)
    }
    return arr.indexOf(cachePath) > -1;
}

self.addEventListener('install', function (event) {
    console.log('Installing Service Worker ...', event);
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                //cache.addAll(urlsToCache);

                urlsToCache.forEach(function(el) {
                    if (el.length > 0) {
                        cache.add(el)
                            .catch(function(err){
                                    console.log("error while caching element: " + err);
                                }
                            );
                    }
                });
            })
    )
});

self.addEventListener('fetch', function (event) {
    let urlTest = inUrlArray(event.request.url, urlsToCache);
    if (urlTest) {
        event.respondWith(caches.open(CACHE_NAME)
            .then(function (cache) {
                return fetch(event.request)
                    .then(function(resp) {
                        cache.put(event.request, resp.clone())
                            .then(function () {
                                console.log("put ok");
                            })
                            .catch(function (err) {
                                console.log("error with put: " + err);
                            });
                        return resp;
                    });
            })
        )
    } else {
        caches.open(CACHE_NAME).then(function (cache) {
            let clonedRq = event.request.clone();
            cache.match(event.request)
                .then(function (response) {
                    if (response) {
                        return response;
                    }
                }).catch(function (err) {
                console.log("no match in cache, going for online: " + err);
            })
        })
    }
});

// self.addEventListener('fetch', function (event) {
//
//     let urlTest = inUrlArray(event.request.url, urlsToCache);
//     if (urlTest) {
//         event.respondWith(
//             caches.open(CACHE_NAME_DYNAMIC)
//                 .then(function (cache) {
//                     return fetch(event.request)
//                         .then(function (res) {
//                             cache.put(event.request.url, res.clone());
//                             return res;
//                         });
//                 })
//         )
//     } else {
//         event.respondWith(
//             caches.match(event.request)
//                 .then(function (response) {
//                     if (response) {
//                         return response;
//                     } else {
//                         return fetch(event.request)
//                             .then(function (res) {
//                                 return caches.open(CACHE_NAME_DYNAMIC)
//                                     .then(function (cache) {
//                                         cache.put(event.request.url, res.clone());
//                                         return res;
//                                     })
//                             })
//                             .catch(function (err) {
//                                 return caches.open(CACHE_NAME)
//                                     .then(function (cache) {
//                                         if (event.request.headers.get('accept').includes('text/html')) {
//                                             return cache.match('/offline.html');
//                                         }
//                                     });
//                             });
//                     }
//                 })
//         );
//     }
// });

// self.addEventListener('activate', function (event) {
//     console.log('Activating Service Worker ....', event);
//     event.waitUntil(
//         caches.keys()
//             .then(function (keyList) {
//                 return Promise.all(keyList.map(function (key) {
//                     // kill old and deactivated sw-caches
//                     if (key !== CACHE_NAME && key !== CACHE_NAME_DYNAMIC) {
//                         console.log('Removing old cache.', key);
//                         return caches.delete(key);
//                     }
//                 }));
//             })
//     );
//     return self.clients.claim();
// });