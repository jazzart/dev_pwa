var urlsToCache = ["./",
    "./index.html",
    "./contact.html",
    "./portfolio.html",
    "./pricing.html",
    "./tour.html"
];
const CACHE_NAME = "cache-vrx3";

self.addEventListener('install', function (event) {
    //console.log('Installing Service Worker ...', event);
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log("addAll");
                return cache.addAll(urlsToCache).then(function () {
                });
            })
    );
});

self.addEventListener('activate', function(e) {
    console.log('[ServiceWorker] Activated');

    e.waitUntil(

        // Get all the cache keys (cacheName)
        caches.keys().then(function(cacheNames) {
            return Promise.all(cacheNames.map(function(thisCacheName) {

                // If a cached item is saved under a previous cacheName
                if (thisCacheName !== cacheName) {

                    // Delete that cached file
                    console.log('[ServiceWorker] Removing Cached Files from Cache - ', thisCacheName);
                    return caches.delete(thisCacheName);
                }
            }));
        })
    ); // end e.waitUntil

});

self.addEventListener('fetch', function (event) {
    caches.open(CACHE_NAME).then(function (cache) {
        event.respondWith(
            cache.match(event.request)
                .then(function (response) {
                    let clonedRq = event.request.clone();
                    if (response) {
                        return response;
                    }
                    return fetch(clonedRq).then(function (resp) {
                        let respClone = resp.clone();
                        caches.open(CACHE_NAME)
                            .then(function (cache) {
                                return cache.put(event.request, respClone).then(function () {
                                });
                            })

                    })
                }).catch(function (err) {
                console.log("fetch error " + err);

            })
        );
    });
});

